﻿using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.DatabaseService.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProjektDokumentVerwaltung
{
    public class MockRepo : IDBRepositoryService
    {
        public void DeleteDocument(Document document)
        {
            throw new NotImplementedException();
        }

        public void DeleteProject(Project project)
        {
            throw new NotImplementedException();
        }

        public Document GetDocumentByID(int id)
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<DocumentType> GetDocumentTypes()
        {
            throw new NotImplementedException();
        }

        public Project GetProjectByID(int id)
        {
            throw new NotImplementedException();
        }

        public List<Document> GetProjectDocuments(int projectID)
        {
            var docList = new List<Document>();

            docList.Add(new Document { Id = 1,
                Name = "Document1",
                DocumentTypeId = 4,
                DocumentType = new DocumentType { Id = 4, Name = "Type4" },
                ProjectId = 1,
                Project = new Project { Id = 1, Name = "Project1" },
                Path = "TestPath1"
            }) ;
            docList.Add(new Document
            {
                Id = 1,
                Name = "Document2",
                DocumentTypeId = 3,
                DocumentType = new DocumentType { Id = 3, Name = "Type3" },
                ProjectId = 2,
                Project = new Project { Id = 2, Name = "Project2" },
                Path = "TestPath2"
            });
            docList.Add(new Document
            {
                Id = 1,
                Name = "Document3",
                DocumentTypeId = 4,
                DocumentType = new DocumentType { Id = 4, Name = "Type4" },
                ProjectId = 1,
                Project = new Project { Id = 1, Name = "Project1" },
                Path = "TestPath3"
            });
            return docList;
        }

        public ObservableCollection<Project> GetProjects()
        {
            var projects = new ObservableCollection<Project>
            {
                new Project{Id = 1, Name = "Trio" },
                new Project{Id = 2, Name = "Terror From The Sky" },
                new Project{Id = 3, Name = "Prime" },
                new Project{Id = 4, Name = "New Age" },
                new Project{Id = 5, Name = "We need heroes" },
                new Project{Id = 6, Name = "Sicaria" },
                new Project{Id = 7, Name = "Spieletisch" },
                new Project{Id = 8, Name = "Gravori Höhlen" },
            };

            return projects;
        }

        public int NewProject(Project p)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDocument(Document doc)
        {
            throw new NotImplementedException();
        }

        public bool UpdateProject(Project p)
        {
            throw new NotImplementedException();
        }
    }
}
