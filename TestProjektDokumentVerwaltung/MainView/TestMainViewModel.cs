﻿using ProjektDokumentVerwaltung.Views;
using ProjektDokumentVerwaltung.DatabaseService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Windows.Input;
using System.Threading;
using ProjektDokumentVerwaltung.Commands;
using Moq;
using ProjektDokumentVerwaltung.Dialogs.DialogService;
using System.Windows;
using ProjektDokumentVerwaltung.DatabaseService;

namespace TestProjektDokumentVerwaltung.MainView
{
    public class TestMainViewModel:IDisposable
    {
        private MockRepo _repo;
        private MainWindowViewModel _mainViewModel;

        public TestMainViewModel()
        {
            _repo = new MockRepo();
            _mainViewModel = new MainWindowViewModel(_repo, new DialogViewModelFactory(_repo), null);
        }

        public void Dispose()
        {
            _mainViewModel = null;
            _repo = null;
        }

        [Fact]
        public void Constructor_ProjectsLoaded()
        {
            Assert.Equal(8, _mainViewModel.Projects.Count);
            Assert.Equal("Trio", _mainViewModel.Projects[0].Name);
        }

        [Fact]
        public void Constructor_ActionCommandsInitialized()
        {
            Assert.Equal(typeof(ActionCommand), _mainViewModel.AddProjectCommand.GetType());
            Assert.Equal(typeof(ActionCommand), _mainViewModel.EditProjectCommand.GetType());
            Assert.Equal(typeof(ActionCommand), _mainViewModel.DeleteProjectCommand.GetType());
            Assert.Equal(typeof(ActionCommand), _mainViewModel.AddDocumentCommand.GetType());
            Assert.Equal(typeof(ActionCommand), _mainViewModel.EditDocumentCommand.GetType());
            Assert.Equal(typeof(ActionCommand), _mainViewModel.DeleteDocumentCommand.GetType());
        }

        [Fact]
        public void SetProject_PropertyChangedEvent()
        {
            _mainViewModel.PropertyChanged += delegate
            {
                Assert.True(true);
            };

            _mainViewModel.Projects = _repo.GetProjects();
        }

        [Fact]
        public void SetSelectProject_FillProjectDocumentsTypesList()
        {
            var testProject = new Project { Id = 1, Name= "Project1" };

            _mainViewModel.SelectedProject = testProject;

            Assert.Equal(2, _mainViewModel.ProjectDocumentsTypes.Count);
            Assert.Equal(2, _mainViewModel.ProjectDocumentsTypes[0].ProjectDocuments.Count);

            Assert.Equal("Document2", _mainViewModel.ProjectDocumentsTypes[1].ProjectDocuments[0].Name);
        }

        [Fact]
        public void SetSelectedProject_Null()
        {
            _mainViewModel.SelectedProject = null;

            Assert.Empty(_mainViewModel.ProjectDocumentsTypes);
        }

        [Fact]
        public void SetSelectedProject_PropertyChangedEvent()
        {
            _mainViewModel.PropertyChanged += delegate
            {
                Assert.True(true);
            };

            _mainViewModel.SelectedProject = null;
        }

        [Fact]
        public void SetSelectedProjectDocument_Null()
        {
            _mainViewModel.DisplayedDocumentPath = "";
            _mainViewModel.SelectedProjectDocument = null;

            Assert.Equal("", _mainViewModel.DisplayedDocumentPath);
        }

        [Fact]
        public void SetSelectedProjectDocument_SetDocumentPath()
        {
            _mainViewModel.DisplayedDocumentPath = "";
            _mainViewModel.SelectedProjectDocument = new Document {Id = 1, Name = "Test1",Path = "TestPath" };

            Assert.Equal("TestPath", _mainViewModel.DisplayedDocumentPath);
        }

        [Fact]
        public void SetSelectedProjectDocument_PropertyChangedEvent()
        {
            _mainViewModel.PropertyChanged += delegate
            {
                Assert.True(true);
            };

            _mainViewModel.SelectedProjectDocument = new Document { Id = 1, Name = "Test1", Path = "TestPath" };
        }

        [Fact]
        public void SetDisplayedDocumentPath_PropertyChangedEvent()
        {
            _mainViewModel.PropertyChanged += delegate
            {
                Assert.True(true);
            };

            _mainViewModel.DisplayedDocumentPath = "TestPath" ;
        }

        [Fact]
        public void AddProjectCommandExcexute_Successful()
        {
            var mockDialogService = new Mock<IDialogService>();
            mockDialogService.Setup(x => x.OpenDialog(It.IsAny<DialogViewModelBase>(),It.IsAny<Window>())).Returns(DialogResult.Yes);

            var mockRepository = new Mock<IDBRepositoryService>();
            mockRepository.Setup(x => x.GetProjects()).Returns(new System.Collections.ObjectModel.ObservableCollection<Project>());

            var viewModel = new MainWindowViewModel(mockRepository.Object, new DialogViewModelFactory(mockRepository.Object), mockDialogService.Object);

            viewModel.AddProjectCommand.Execute(null);

            mockRepository.Verify(x => x.GetProjects(),Times.Exactly(2));
        }

        [Fact]
        public void AddProjectCommandExcexute_NotSuccessful()
        {
            var mockDialogService = new Mock<IDialogService>();
            mockDialogService.Setup(x => x.OpenDialog(It.IsAny<DialogViewModelBase>(), It.IsAny<Window>())).Returns(DialogResult.No);

            var mockRepository = new Mock<IDBRepositoryService>();
            mockRepository.Setup(x => x.GetProjects()).Returns(new System.Collections.ObjectModel.ObservableCollection<Project>());

            var viewModel = new MainWindowViewModel(mockRepository.Object, new DialogViewModelFactory(mockRepository.Object), mockDialogService.Object);

            viewModel.AddProjectCommand.Execute(null);

            mockRepository.Verify(x => x.GetProjects(), Times.Once);
        }

        [Fact]
        public void DeleteProjectCommandExcexute_Yes()
        {
            var mockDialogService = new Mock<IDialogService>();
            mockDialogService.Setup(x => x.OpenDialog(It.IsAny<DialogViewModelBase>(), It.IsAny<Window>())).Returns(DialogResult.Yes);

            var mockRepository = new Mock<IDBRepositoryService>();
            mockRepository.Setup(x => x.GetProjects()).Returns(new System.Collections.ObjectModel.ObservableCollection<Project>());
            mockRepository.Setup(x => x.DeleteProject(It.IsAny<Project>()));

            var viewModel = new MainWindowViewModel(mockRepository.Object, new DialogViewModelFactory(mockRepository.Object), mockDialogService.Object);

            viewModel.DeleteProjectCommand.Execute(null);

            mockRepository.Verify(x => x.GetProjects(), Times.Exactly(2));
            mockRepository.Verify(x => x.DeleteProject(It.IsAny<Project>()), Times.Once);
        }

        [Fact]
        public void DeleteProjectCommandExcexute_No()
        {
            var mockDialogService = new Mock<IDialogService>();
            mockDialogService.Setup(x => x.OpenDialog(It.IsAny<DialogViewModelBase>(), It.IsAny<Window>())).Returns(DialogResult.No);

            var mockRepository = new Mock<IDBRepositoryService>();
            mockRepository.Setup(x => x.GetProjects()).Returns(new System.Collections.ObjectModel.ObservableCollection<Project>());
            mockRepository.Setup(x => x.DeleteProject(It.IsAny<Project>()));

            var viewModel = new MainWindowViewModel(mockRepository.Object, new DialogViewModelFactory(mockRepository.Object), mockDialogService.Object);

            viewModel.DeleteProjectCommand.Execute(null);

            mockRepository.Verify(x => x.GetProjects(), Times.Once);
            mockRepository.Verify(x => x.DeleteProject(It.IsAny<Project>()), Times.Never);
        }
    }
}
