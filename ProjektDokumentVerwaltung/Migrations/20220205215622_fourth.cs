﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjektDokumentVerwaltung.Migrations
{
    public partial class fourth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Projects_projectId",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "projectId",
                table: "Documents",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_projectId",
                table: "Documents",
                newName: "IX_Documents_ProjectId");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Path",
                value: "Y:\\GameProjekte\\WeNeedHereos\\Beschreibung.pdf");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Projects_ProjectId",
                table: "Documents",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Projects_ProjectId",
                table: "Documents");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Documents",
                newName: "projectId");

            migrationBuilder.RenameIndex(
                name: "IX_Documents_ProjectId",
                table: "Documents",
                newName: "IX_Documents_projectId");

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Path",
                value: "Y:\\GameProjekte\\WeNeedHereos\\Beschreibung.odt");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Projects_projectId",
                table: "Documents",
                column: "projectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
