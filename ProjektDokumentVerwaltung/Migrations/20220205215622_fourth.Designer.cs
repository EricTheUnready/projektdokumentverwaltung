﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProjektDokumentVerwaltung.DatabaseService;

namespace ProjektDokumentVerwaltung.Migrations
{
    [DbContext(typeof(ProjectDokumentsDBContext))]
    [Migration("20220205215622_fourth")]
    partial class fourth
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.13");

            modelBuilder.Entity("ProjektDokumentVerwaltung.DatabaseService.Models.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DocumentTypeId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("ProjectId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("DocumentTypeId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Documents");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DocumentTypeId = 4,
                            Name = "Gamescreen",
                            Path = "Y:\\GameProjekte\\WeNeedHereos\\GameScreen.png",
                            ProjectId = 5
                        },
                        new
                        {
                            Id = 2,
                            DocumentTypeId = 1,
                            Name = "Spiel Idee",
                            Path = "Y:\\GameProjekte\\WeNeedHereos\\Beschreibung.pdf",
                            ProjectId = 5
                        });
                });

            modelBuilder.Entity("ProjektDokumentVerwaltung.DatabaseService.Models.DocumentType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("DocumentTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Idee"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Geschichte"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Spiele Mechaniken"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Darstellungs Ideen"
                        },
                        new
                        {
                            Id = 5,
                            Name = "Zeichnungen"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Sonstiges"
                        });
                });

            modelBuilder.Entity("ProjektDokumentVerwaltung.DatabaseService.Models.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Trio"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Terror From The Sky"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Prime"
                        },
                        new
                        {
                            Id = 4,
                            Name = "New Age"
                        },
                        new
                        {
                            Id = 5,
                            Name = "We need heroes"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Sicaria"
                        },
                        new
                        {
                            Id = 7,
                            Name = "Spieletisch"
                        },
                        new
                        {
                            Id = 8,
                            Name = "Gravori Höhlen"
                        });
                });

            modelBuilder.Entity("ProjektDokumentVerwaltung.DatabaseService.Models.Document", b =>
                {
                    b.HasOne("ProjektDokumentVerwaltung.DatabaseService.Models.DocumentType", "DocumentType")
                        .WithMany()
                        .HasForeignKey("DocumentTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ProjektDokumentVerwaltung.DatabaseService.Models.Project", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("DocumentType");

                    b.Navigation("Project");
                });
#pragma warning restore 612, 618
        }
    }
}
