﻿using ProjektDokumentVerwaltung.DatabaseService.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProjektDokumentVerwaltung.DatabaseService
{
    public interface IDBRepositoryService
    {
        ObservableCollection<Models.Project> GetProjects();
        List<Models.Document> GetProjectDocuments(int projectID);

        void DeleteProject(Models.Project project);

        Project GetProjectByID(int id);

        int NewProject(Project p);
        bool UpdateProject(Project p);

        Document GetDocumentByID(int id);

        ObservableCollection<DocumentType> GetDocumentTypes();

        bool UpdateDocument(Document doc);

        void DeleteDocument(Document document);
    }
}