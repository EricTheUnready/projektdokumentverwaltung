﻿using ProjektDokumentVerwaltung.DatabaseService.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ProjektDokumentVerwaltung.DatabaseService
{
    public class DBRepositoryService : IDBRepositoryService
    {
        ProjectDokumentsDBContext dbContext;

        public DBRepositoryService(ProjectDokumentsDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ObservableCollection<Project> GetProjects()
        {
            var projects = dbContext.Projects.OrderBy(x => x.Name);

            return new ObservableCollection<Project>(projects);
        }

        public List<Models.Document> GetProjectDocuments(int projectID)
        {
            var documents = dbContext.Documents
                .Include(doc => doc.DocumentType)
                .Include(doc => doc.Project)
                .Where(doc => doc.ProjectId == projectID).OrderBy(doc => doc.Name);

            return documents.ToList<Models.Document>();
        }

        public void DeleteProject(Project project)
        {
            dbContext.Projects.Remove(project);

            dbContext.SaveChanges();
        }

        public Project GetProjectByID(int id)
        {
            return dbContext.Projects.Find(id);
        }

        public int NewProject(Project p)
        {
            dbContext.Projects.Add(p);

            dbContext.SaveChanges();
            return p.Id;
        }

        public bool UpdateProject(Project p)
        {
            dbContext.Update(p);

            return dbContext.SaveChanges()!= 0;
        }

        public Document GetDocumentByID(int id)
        {
            return dbContext.Documents.Find(id);
        }

        public ObservableCollection<DocumentType> GetDocumentTypes()
        {
            var documentTypes = dbContext.DocumentTypes.OrderBy(x => x.Name);

            return new ObservableCollection<DocumentType>(documentTypes);
        }

        public bool UpdateDocument(Document doc)
        {
            dbContext.Update(doc);

            return dbContext.SaveChanges() != 0;
        }

        public void DeleteDocument(Document document)
        {
            dbContext.Documents.Remove(document);

            dbContext.SaveChanges();
        }
    }
}
