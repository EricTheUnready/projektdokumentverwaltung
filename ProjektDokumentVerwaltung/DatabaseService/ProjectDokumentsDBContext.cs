﻿using Microsoft.EntityFrameworkCore;
using ProjektDokumentVerwaltung.DatabaseService.Models;

namespace ProjektDokumentVerwaltung.DatabaseService
{
    public class ProjectDokumentsDBContext: DbContext
    {
        //TODO ConnectionString aus Konfigurationsdatei
        protected override void OnConfiguring(DbContextOptionsBuilder options)
    => options.UseSqlite("Data Source=project_documents.db");

        public DbSet<Project> Projects { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Models.Document> Documents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var documentTypes = new DocumentType[] 
            {           
                new DocumentType{Id = 1, Name = "Idee" },
                new DocumentType{Id = 2, Name = "Geschichte" },
                new DocumentType{Id = 3, Name = "Spiele Mechaniken" },
                new DocumentType{Id = 4, Name = "Darstellungs Ideen" },
                new DocumentType{Id = 5, Name = "Zeichnungen" },
                new DocumentType{Id = 6, Name = "Sonstiges" }
            };
            modelBuilder.Entity<DocumentType>().HasData(documentTypes);

            base.OnModelCreating(modelBuilder);
        }
    }
}
