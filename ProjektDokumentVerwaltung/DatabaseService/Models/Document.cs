﻿using System.ComponentModel.DataAnnotations;

namespace ProjektDokumentVerwaltung.DatabaseService.Models
{
    public class Document: IEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        public int DocumentTypeId { get; set; }
        public virtual DocumentType DocumentType { get; set; }

        [Required]
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Path { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
