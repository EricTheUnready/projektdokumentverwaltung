﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektDokumentVerwaltung.DatabaseService.Models
{
    public class DocumentType : IEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
