﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ProjektDokumentVerwaltung.Helper
{
    
    public static class WebBrowserUtility
    {
        public static readonly DependencyProperty BindableSourceProperty =
            DependencyProperty.RegisterAttached("BindableSource", typeof(string), typeof(WebBrowserUtility), new UIPropertyMetadata(null, BindableSourcePropertyChanged));

        public static string GetBindableSource(DependencyObject obj)
        {
            return (string)obj.GetValue(BindableSourceProperty);
        }

        public static void SetBindableSource(DependencyObject obj, string value)
        {
            obj.SetValue(BindableSourceProperty, value);
        }

        public static void BindableSourcePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o is not WebBrowser browser) return;

            Uri uri = null;
            String url = "";

            if (e.NewValue is string)
            {
                var uriString = e.NewValue as string;
                uri = string.IsNullOrWhiteSpace(uriString) ? null : new Uri(uriString);
            }
            else if (e.NewValue is Uri)
            {
                uri = e.NewValue as Uri;
            }
            url = System.Web.HttpUtility.UrlPathEncode(e.NewValue as string);

            browser.Navigate(url);

            if (url.Contains(".pdf"))
                browser.NavigateToString("<html><body><h1>Dokument wird in einem externem Programm dargestellt.</h1></body></html>");
        }

        private static void NavigateWebBrowserToDocObjUrl(string url, WebBrowser browser)
        {
            browser.NavigateToString("<html><head><meta http-equiv=\"X - UA - Compatible\" content =\"IE = edge\" /></head><body>  <iframe src=\"" + url + "\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\" width=\"100%\" height=\"100%\" scrolling=\"auto\"></iframe> </body> </html>");
        }
    }
}
