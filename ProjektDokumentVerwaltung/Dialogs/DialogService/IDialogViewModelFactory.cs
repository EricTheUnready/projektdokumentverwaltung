﻿using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.Dialogs.DialogYesNo;

namespace ProjektDokumentVerwaltung.Dialogs.DialogService
{
    public interface IDialogViewModelFactory
    {
        IDBRepositoryService Repository { get; }

        DialogYesNo.ViewModel GetYesNoDialogViewModel(string question);

        DialogProject.ViewModel GetProjectDialogViewModel(string title, int? id);

        DialogProject.ViewModel GetProjectDialogViewModel(string title);

        DialogDocument.ViewModel GetDocumentDialogWindow(string title);

        DialogDocument.ViewModel GetDocumentDialogWindow(string title, int? id);
    }
}