﻿using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.Dialogs.DialogProject;
using ProjektDokumentVerwaltung.Dialogs.DialogYesNo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProjektDokumentVerwaltung.Dialogs.DialogService
{

    public class DialogViewModelFactory : IDialogViewModelFactory
    {
        public DialogViewModelFactory(IDBRepositoryService repo)
        {
            Repository = repo;
        }

        public IDBRepositoryService Repository { get; }

        public DialogDocument.ViewModel GetDocumentDialogWindow(string title,int? docId)
        {
            return new DialogDocument.ViewModel(title,Repository, docId);
        }

        public DialogDocument.ViewModel GetDocumentDialogWindow(string title)
        {
            return GetDocumentDialogWindow(title, null);
        }

        public DialogProject.ViewModel GetProjectDialogViewModel(string title)
        {
            return GetProjectDialogViewModel(title, null);
        }

        public DialogProject.ViewModel GetProjectDialogViewModel(string title, int? id = null)
        {
            return new DialogProject.ViewModel(title, Repository, id);
        }

        public DialogYesNo.ViewModel GetYesNoDialogViewModel(string question)
        {
            return new DialogYesNo.ViewModel(question);
        }

        
    }
}
