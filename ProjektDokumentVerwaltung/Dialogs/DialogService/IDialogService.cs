﻿using System.Windows;

namespace ProjektDokumentVerwaltung.Dialogs.DialogService
{
    public interface IDialogService
    {
        DialogResult OpenDialog(DialogViewModelBase vm, Window owner);
    }
}