﻿using ProjektDokumentVerwaltung.Dialogs.DialogService;
using ProjektDokumentVerwaltung.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ProjektDokumentVerwaltung.Dialogs.DialogYesNo
{
    public class ViewModel : DialogViewModelBase
    {
        private ICommand yesCommand = null;
        public ICommand YesCommand
        {
            get { return yesCommand; }
            set { yesCommand = value; }
        }

        private ICommand noCommand = null;
        public ICommand NoCommand
        {
            get { return noCommand; }
            set { noCommand = value; }
        }

        public ViewModel(string message)
            : base(message)
        {
            yesCommand = new ActionCommand(OnYesClicked);
            noCommand = new ActionCommand(OnNoClicked);
        }

        private void OnYesClicked(object parameter)
        {
            CloseDialogWithResult(parameter as Window, DialogResult.Yes);
        }

        private void OnNoClicked(object parameter)
        {
            CloseDialogWithResult(parameter as Window, DialogResult.No);
        }
    }
}
