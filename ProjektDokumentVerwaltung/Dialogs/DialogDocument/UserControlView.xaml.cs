﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ProjektDokumentVerwaltung.Dialogs.DialogDocument
{
    /// <summary>
    /// Interaktionslogik für UserControl.xaml
    /// </summary>
    public partial class UserControlView : UserControl
    {
        public UserControlView()
        {
            InitializeComponent();
        }

        private void OpenFileDialog_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                var filename = openFileDialog.FileName;
                filePathTextbox.Text = filename;
                filePathTextbox.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            }
        }
    }
}
