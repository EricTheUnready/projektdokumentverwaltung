﻿using ProjektDokumentVerwaltung.Commands;
using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.DatabaseService.Models;
using ProjektDokumentVerwaltung.Dialogs.DialogService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProjektDokumentVerwaltung.Dialogs.DialogDocument
{
    public class ViewModel : DialogViewModelBase
    {
        private readonly IDBRepositoryService repository;
        public Document DocumentModel { get; set; }

        public ObservableCollection<DocumentType> DocumentTypes { get; set; }

        public ObservableCollection<Project> Projects { get; set; }
        public ActionCommand SaveCommand { get; }
        public ActionCommand CancelCommand { get; }

        public ViewModel(String title, DatabaseService.IDBRepositoryService repo, int? Id) : base(title)
        {
            repository = repo;
            if (Id != null)
            {
                DocumentModel = repository.GetDocumentByID(Id.Value);
            }
            else
            {
                DocumentModel = new Document();
            }

            DocumentTypes = repository.GetDocumentTypes();
            Projects = repository.GetProjects();


            SaveCommand = new ActionCommand(OnSave, OnSaveCanExecute);
            CancelCommand = new ActionCommand(OnCancel);
        }

        private bool OnSaveCanExecute(object Parameter)
        {
            if (DocumentModel.Name != null
                && DocumentModel.Path != null
                && DocumentModel.Project != null
                && DocumentModel.DocumentType != null)
                return true;

            return false;
        }

        private void OnCancel(object Parameter)
        {
            this.CloseDialogWithResult(Parameter as Window, DialogResult.No);
        }

        private void OnSave(object Parameter)
        {
            if (repository.UpdateDocument(DocumentModel))
                this.CloseDialogWithResult(Parameter as Window, DialogResult.Yes);
            else
                this.CloseDialogWithResult(Parameter as Window, DialogResult.No);
        }

    }
}
