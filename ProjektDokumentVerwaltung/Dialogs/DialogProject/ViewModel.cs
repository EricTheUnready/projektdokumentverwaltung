﻿using ProjektDokumentVerwaltung.Commands;
using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.DatabaseService.Models;
using ProjektDokumentVerwaltung.Dialogs.DialogService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProjektDokumentVerwaltung.Dialogs.DialogProject
{
    public class ViewModel: DialogViewModelBase
    {
        private readonly IDBRepositoryService repository;

        public Project ProjectModel { get; set; }
        public ActionCommand SaveCommand { get; }
        public ActionCommand CancelCommand { get; }

        public ViewModel(String title, DatabaseService.IDBRepositoryService rep, int? Id): base (title)
        {
            repository = rep;
            if (Id != null)
            {
                ProjectModel = repository.GetProjectByID(Id.Value);
            }
            else
                ProjectModel = new Project();

            SaveCommand = new ActionCommand(OnSave,OnSaveCanExecute);
            CancelCommand = new ActionCommand(OnCancel);
        }

        void OnSave(object Parameter)
        {
            if (repository.UpdateProject(ProjectModel))
                this.CloseDialogWithResult(Parameter as Window, DialogResult.Yes);
            else
                this.CloseDialogWithResult(Parameter as Window, DialogResult.No);
        }

        bool OnSaveCanExecute(object Parameter)
        {
            return true;
        }
        void OnCancel(object Parameter)
        {
            this.CloseDialogWithResult(Parameter as Window, DialogResult.No);
        }
    }
}
