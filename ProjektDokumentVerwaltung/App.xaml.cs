﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.Views;
using ProjektDokumentVerwaltung.Dialogs.DialogService;

namespace ProjektDokumentVerwaltung
{


    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MainWindow window;
        private MainWindowViewModel viewModel;

        public static IServiceProvider Service { get; private set; }

        //private readonly IHost host;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var host = CreateHostBuilder().Build();

            createMainView().ShowDialog();
        }

        static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices((hostBuilderContext, services) =>
                {
                    services.AddDbContext<ProjectDokumentsDBContext>();
                    services.AddTransient<IDBRepositoryService, DBRepositoryService>();
                    services.AddTransient<MainWindowViewModel>();
                    services.AddSingleton<MainWindow>();
                    services.AddSingleton<IDialogViewModelFactory, DialogViewModelFactory>();
                    services.AddSingleton<IDialogService, DialogService>();

                    Service = services.BuildServiceProvider();
                });
        }

        private MainWindow createMainView()
        {
            window = Service.GetService<MainWindow>();
            viewModel = Service.GetService<MainWindowViewModel>();
            window.DataContext = viewModel;
            return window;
        }
    }
}
