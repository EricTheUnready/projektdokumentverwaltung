﻿using ProjektDokumentVerwaltung.DatabaseService.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektDokumentVerwaltung.Views
{
    public class ProjectDocumentType
    {
        public string Name { get; set; }
        public ObservableCollection<Document> ProjectDocuments { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
