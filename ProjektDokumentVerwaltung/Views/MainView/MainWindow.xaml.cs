﻿using ProjektDokumentVerwaltung.DatabaseService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjektDokumentVerwaltung.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
                ((MainWindowViewModel)this.DataContext).SelectedProjectDocument = null;
            else if (e.NewValue.GetType() == typeof(Document))
            {
                ((MainWindowViewModel)this.DataContext).SelectedProjectDocument = (Document)e.NewValue;
            }
        }
    }
}
