﻿using ProjektDokumentVerwaltung.Commands;
using ProjektDokumentVerwaltung.Dialogs.DialogService;
using ProjektDokumentVerwaltung.DatabaseService;
using ProjektDokumentVerwaltung.DatabaseService.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProjektDokumentVerwaltung.Views
{
    public class MainWindowViewModel : IMainWindowViewModel
    {
        private ObservableCollection<Project> _projects;
        private IDBRepositoryService _dbRepository;
        private readonly IDialogViewModelFactory _dialogFactory;
        private readonly IDialogService _dialogService;
        private ObservableCollection<ProjectDocumentType> _projectDocumentTypes;
        private string _displayedDocumentPath;
        private Project _selectedProject;
        private Document _selectedProjectDocument;
        private ICommand _openDialogCommand = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowViewModel(IDBRepositoryService repo, IDialogViewModelFactory dialogFactory, IDialogService dialogService)
        {
            //TODO aus Datenbank über Service beziehen
            _projects = repo.GetProjects();
            _projectDocumentTypes = new ObservableCollection<ProjectDocumentType>();
            _dbRepository = repo;
            _dialogFactory = dialogFactory;
            _dialogService = dialogService;
            DeleteProjectCommand = new ActionCommand(OnDeleteProjectExecute,OnDeleteProjectCanExecute);
            AddProjectCommand = new ActionCommand(OnOpenAddProjectDialog);
            EditProjectCommand = new ActionCommand(OnOpenEditProjectDialog, OnDeleteProjectCanExecute);

            AddDocumentCommand = new ActionCommand(OnOpenAddDocumentDialog);
            DeleteDocumentCommand = new ActionCommand(OnOpenDeleteDocumentDialog, OnDeleteDocumentCanExecute);
            EditDocumentCommand = new ActionCommand(OnOpenEditDocumentDialog, OnDeleteDocumentCanExecute);
        }

        public MainWindowViewModel()
        {

        }

        #region Binding Properties
        public ObservableCollection<Project> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                RaisePropertyChanged("Projects");
            }
        }
        public ICommand OpenDialogCommand
        {
            get { return _openDialogCommand; }
            set { _openDialogCommand = value; }
        }
        public ObservableCollection<ProjectDocumentType> ProjectDocumentsTypes
        {
            get { return _projectDocumentTypes; }
        }
        public Project SelectedProject
        {
            get => _selectedProject;
            set
            {
                _projectDocumentTypes.Clear();

                if (value != null)
                    mappingDocuments(_dbRepository.GetProjectDocuments(value.Id));

                _selectedProject = value;

                RaisePropertyChanged("SelectedProject");
            }
        }
        public Document SelectedProjectDocument 
        { 
            get => _selectedProjectDocument;
            set
            { 
                _selectedProjectDocument = value;
                if(value != null)
                    DisplayedDocumentPath = value.Path;

                RaisePropertyChanged("SelectedProjectDocument");
            }
        }
        public string DisplayedDocumentPath
        {
            get => _displayedDocumentPath;
            set
            {
                _displayedDocumentPath = value;
                RaisePropertyChanged("DisplayedDocumentPath");
            }
        }
        #endregion

        public void RaisePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void mappingDocuments(List<Document> documents)
        {
            //TODO das sollte besser/einfacher/übersichtlicher gehen
            foreach (var document in documents)
            {
                IEnumerable<ProjectDocumentType> projectDocTypeList = _projectDocumentTypes.Where(x => x.Name == document.DocumentType.Name);
                ProjectDocumentType projectDocType;

                if (!projectDocTypeList.Any())
                {
                    projectDocType = new ProjectDocumentType { Name = document.DocumentType.Name, ProjectDocuments = new ObservableCollection<Document>() };
                    ProjectDocumentsTypes.Add(projectDocType);
                }
                else
                {
                    projectDocType = projectDocTypeList.First();
                }

                projectDocType.ProjectDocuments.Add(document);
            }
        }

        #region Project Command Methods
        public ICommand DeleteProjectCommand { get; }
        public ICommand AddProjectCommand { get; }
        public ICommand EditProjectCommand { get; private set; }
        void OnDeleteProjectExecute(object Parameter)
        {
            var viewModel = _dialogFactory.GetYesNoDialogViewModel( "Wollen sie das Project wirklich löschen?");
            var result = _dialogService.OpenDialog(viewModel, Parameter as Window);
            
            if (result == Dialogs.DialogService.DialogResult.Yes)
            {
                _dbRepository.DeleteProject(SelectedProject);

                Projects = _dbRepository.GetProjects();
            }
        }

        bool OnDeleteProjectCanExecute(object Parameter)
        {

            if (SelectedProject != null )
                return true;

            return false;
        }

        private void OnOpenAddProjectDialog(object Parameter)
        {
            Dialogs.DialogService.DialogViewModelBase viewModel = _dialogFactory.GetProjectDialogViewModel("Projekt hinzufügen");
            Dialogs.DialogService.DialogResult result = _dialogService.OpenDialog(viewModel, Parameter as Window);
            
            if (result == Dialogs.DialogService.DialogResult.Yes)
                Projects = _dbRepository.GetProjects();
        }

        private void OnOpenEditProjectDialog(object Parameter)
        {
            Dialogs.DialogService.DialogViewModelBase viewModel = _dialogFactory.GetProjectDialogViewModel("Projekt hinzufügen",SelectedProject.Id);
            Dialogs.DialogService.DialogResult result = _dialogService.OpenDialog(viewModel, Parameter as Window);
            
            if (result == Dialogs.DialogService.DialogResult.Yes)
                Projects = _dbRepository.GetProjects();
        }
        #endregion

        #region Document Command Methods
        public ICommand AddDocumentCommand { get; }
        public ICommand DeleteDocumentCommand { get; }
        public ICommand EditDocumentCommand { get; }

        private void OnOpenAddDocumentDialog(object Parameter)
        {
            DialogViewModelBase viewModel = _dialogFactory.GetDocumentDialogWindow("Dokument hinzufügen");
            DialogResult result = _dialogService.OpenDialog(viewModel, Parameter as Window);

            if (result == Dialogs.DialogService.DialogResult.Yes)
            {
                _projectDocumentTypes.Clear();
                mappingDocuments(_dbRepository.GetProjectDocuments(SelectedProject.Id));
            }
        }

        private bool OnDeleteDocumentCanExecute(object arg)
        {
            return SelectedProjectDocument != null;
        }

        private void OnOpenDeleteDocumentDialog(object Parameter)
        {
            var viewModel = _dialogFactory.GetYesNoDialogViewModel("Wollen sie das Document wirklich löschen?");
            var result = _dialogService.OpenDialog(viewModel, Parameter as Window);

            if (result == Dialogs.DialogService.DialogResult.Yes)
            {
                _dbRepository.DeleteDocument(SelectedProjectDocument);

                _projectDocumentTypes.Clear();

                mappingDocuments(_dbRepository.GetProjectDocuments(SelectedProject.Id));
            }
        }

        private void OnOpenEditDocumentDialog(object Parameter)
        {
            Dialogs.DialogService.DialogViewModelBase viewModel = _dialogFactory.GetDocumentDialogWindow("Projekt hinzufügen", SelectedProjectDocument.Id);
            Dialogs.DialogService.DialogResult result = _dialogService.OpenDialog(viewModel, Parameter as Window);

            if (result == Dialogs.DialogService.DialogResult.Yes)
            {
                _projectDocumentTypes.Clear();
                mappingDocuments(_dbRepository.GetProjectDocuments(SelectedProject.Id));
            }
        }
        #endregion
    }
}