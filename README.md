# ProjektDokumentVerwaltung


## Aufgabe des Projekt

Diese Projekt soll als Beispiel dienen, wie man eine c# WPF Anwendung mit Datenbank Anbindung aufbaut. Bis jetzt wurde versucht das MVVM-Pattern, IOC-Container, Entity Framework, Unit Tests mit Moq, einzubinden. Verbesserungsvorschläge sind erwünscht.

## Programmbeschreibung

Mit diesem Programm kann man Dokumente zu Projekten verwalten. Zu jedem Projekt kann es n Dokumente geben. Die Dokumente sind in Typen gruppiert. Zur Zeit ist es möglich Projekte hinzuzufügen, zu ändern und zu löschen. Dokumente können hinzugefügt, gelöscht und ihre Metadaten bearbeitet werden.

## Roadmap Programm Funktionen
- [ ] Dokument Typen verwalten
- [ ] Projektordner, in denen die Dokumente gespeichert werden
- [ ] Versionierung der Dokumente mit Verwaltung
- [ ] Backup

## Roadmap Technische Konzepte
- [ ] Quellcode Dokumentation (nicht was sondern warum)
- [ ] höhere Testabdeckung mit Unit Tests
- [ ] [Unit of work pattern](https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application)  
- [ ] User Acceptance Tests mit Gherkin und Cucumber
- [ ] Asynchrone Programmierung
- [ ] Logging z.B. mit Serilog
- [ ] Programm Konfiguration mit IHostBuilder, z.B. für Datenbank ConnectionString, Serilog konfiguration
- [ ] Programm in mehrere Projekte unterteilen

## Diskussion
Bei folgeden Themen bestehen Unklarheiten:
- MVVM
    - Dialoge über Events im Code behind starten, oder über einen DialogService. Zur Zeit sind beide Varianten implementiert. Über einen Dialog Service werden allerdings die meistens gestartet. Nur ein OpenFileDialog wird über ein Event im Code behind gestartet.Zu dem Thema [hier](https://stackoverflow.com/questions/1043918/open-file-dialog-mvvm/64861760#64861760) und [hier](https://chat.stackoverflow.com/rooms/224729/discussion-on-answer-by-bioniccode-open-file-dialog-mvvm) eine interessante Diskusion.
    - Eine andere Frage ist, ob das Property SelectedProject in ein ViewModel gehört oder nicht, da es auch eine Funktion der View ist und somit weiß das ViewModel etwas über die View. Hier könnte man einfach ein Command ReloadDocuments mit dem Project als Parameter ausführen aus dem CodeBehind.
 - Projektstruktur
    - Ordner nach technischen Elementen aufbauen (Views, ViewModels, Models, etc.) oder nach [Features](https://spin.atomicobject.com/2015/11/18/feature-oriented-c-sharp-structure/). [Hier noch ein Link](https://www.planetgeek.ch/2012/01/25/3077/)zu dem Thema.
    - Eigene Projekte für z.B. die Datenbank Anbindung (wie aufteilen).
    - Benennung von Namespaces und Klassen z.B. Dialogs.Project.ViewModel oder Dialogs.DialogProject.DialogProjectViewModel. Zur Zeit ist es leider eine Mischung von beiden. Allerdings haben hier Unternehmen wahrscheinlich auch Coding Guidlines, wo dieses festgelegt ist.


## Contributing
Jede Konstruktive Kritik und jede Verbesserung ist willkommen. Allerdings gibt es verschiedene Meinungen zu den Themen, siehe links zu dem Thema Dialoge und MVVM und es sollte sich auf eine Umsetzung festgelegt werden. 

## License
[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

